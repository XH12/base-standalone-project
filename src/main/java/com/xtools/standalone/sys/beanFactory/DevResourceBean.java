package com.xtools.standalone.sys.beanFactory;

import com.xtools.standalone.sys.logUtils.WithLoggerImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import javax.annotation.PostConstruct;

/**
 * @author xethhung
 * Setup for mapping dev configuration files
 * @date 11/24/2017
 */
@Configuration
@Profile("dev")
@PropertySource("classpath:configuration/application-dev.properties")
public class DevResourceBean extends WithLoggerImpl {
    @PostConstruct
    public void init(){
        logger.info("Initialized dev resource bean factory completed");
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
