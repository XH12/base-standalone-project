package com.xtools.standalone.sys.beanFactory;

import com.xtools.standalone.sys.logUtils.WithLoggerImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import javax.annotation.PostConstruct;

/**
 * @author xethhung
 * Setup for mapping prod configuration files
 * @date 11/24/2017
 */
@Profile("prod")
@Configuration
@PropertySource("classpath:configuration/application-prod.properties")
public class ProdResourceBean extends WithLoggerImpl {
    @PostConstruct
    public void init(){
        logger.info("Initialized prod resource bean factory completed");
    }
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
