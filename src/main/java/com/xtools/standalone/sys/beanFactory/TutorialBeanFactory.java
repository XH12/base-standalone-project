package com.xtools.standalone.sys.beanFactory;


import com.xtools.standalone.sys.logUtils.WithLoggerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author xethhung
 * This bean factory only contains tutorial coding and is separated by the none used profile
 * @date 11/23/2017
 */
@Configuration
public class TutorialBeanFactory extends WithLoggerImpl {

    @Autowired
    private Environment env;

    @PostConstruct
    public void init(){
        logger.info("Initialized common bean factory completed");
        System.out.println("System home");
        System.out.println(System.getProperty("user.home"));
    }

    @Profile("THIS_IS_TESTING_EXAMPLE")
    @Bean
    public Connection exampleOracleJDBC() throws ClassNotFoundException, SQLException {
        System.out.println("-------- Oracle JDBC Connection Testing ------");

        try {

            Class.forName("oracle.jdbc.driver.OracleDriver");

        } catch (ClassNotFoundException e) {

            logger.info("JDBC Driver not found",e);
            throw e;
        }
        System.out.println("Oracle JDBC Driver Registered!");

        Connection connection = null;

        try {

            connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@localhost:1521:xe", "system", "password");
            return connection;

        } catch (SQLException e) {
            logger.info("Connection Failed! Check output console",e);
            throw e;
        }
    }

    @Profile("THIS_IS_TESTING_EXAMPLE")
    @Bean
    public Connection exampleMysqlJDBC() throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException {
        System.out.println("-------- Oracle JDBC Connection Testing ------");

        try {

            Class.forName("com.mysql.jdbc.Driver").newInstance();

        } catch (ClassNotFoundException e) {

            logger.info("JDBC Driver not found",e);
            throw e;
        } catch (IllegalAccessException e) {
            logger.info(e.getMessage(),e);
            throw e;
        } catch (InstantiationException e) {
            logger.error(e.getMessage(),e);
            throw e;
        }
        System.out.println("Oracle JDBC Driver Registered!");

        Connection connection = null;

        try {

            connection =
                    DriverManager.getConnection("jdbc:mysql://localhost/test?" +
                            "user=minty&password=greatsqldb");
            return connection;

        } catch (SQLException e) {
            logger.info("Connection Failed! Check output console",e);
            throw e;
        }
    }

}
