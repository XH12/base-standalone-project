package com.xtools.standalone.sys;

import com.xtools.extractor.command.CommandExtractor4j;
import com.xtools.extractor.command.CommandExtractor4jBuilder;
import com.xtools.standalone.sys.logUtils.WithLoggerImpl;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication(scanBasePackages = {"com.xtools.standalone"})
public class App extends WithLoggerImpl implements CommandLineRunner
{
    @Autowired
    private ContextAware contextAware;

    public static CommandExtractor4j extractor;

    public static void main( String[] args )
    {
        extractor = CommandExtractor4jBuilder.build(args);
        if(extractor.named("logName")!=null) {
            String logFilename = extractor.named("logName")==""?"app.log":extractor.named("logName");
            System.setProperty("logName", logFilename);
        }
        else
            System.setProperty("logName","app.log");
        if(extractor.named("logPath")!=null){
            String logPath = extractor.named("logPath");
            logPath = logPath.startsWith(".")?System.getProperty("user.home")+logPath.substring(1):logPath;
            System.setProperty("logPath",logPath);
        }
        else
            System.setProperty("logPath",System.getProperty("user.home"));

        org.apache.logging.log4j.core.LoggerContext ctx =
                (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
        ctx.reconfigure();

        Logger logger = LoggerFactory.getLogger(App.class);
        logger.info("Start xtools Standalone application");
        SpringApplication app = new SpringApplication(App.class);

        if(extractor.hasFlag("prod"))
            app.setAdditionalProfiles("prod");
        else
            app.setAdditionalProfiles("dev");

        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }
    @Override
    public void run(String... args) throws Exception {
        StandaloneTask bean = (StandaloneTask)contextAware.context.getBean(extractor.named("task"));
        int code = bean.execute(extractor);
        logger.info("End xtools Standalone application with return code: " + code);
    }
}
