package com.xtools.standalone.sys.logUtils;

import com.xtools.standalone.sys.logUtils.WithLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author xethhung
 * @date 11/23/2017
 */
public abstract class WithLoggerImpl implements WithLogger {
    protected Logger logger = getLogger(this.getClass());
    @Override
    public Logger getLogger(Class<?> clazz) {
        return LoggerFactory.getLogger(this.getClass());
    }
}
