package com.xtools.standalone.sys.logUtils;

import org.slf4j.Logger;

/**
 * @author xethhung
 * @date 11/23/2017
 */
public interface WithLogger {
    Logger getLogger(Class<?> clazz);
}
