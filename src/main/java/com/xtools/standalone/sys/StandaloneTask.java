package com.xtools.standalone.sys;

import com.xtools.extractor.command.CommandExtractor4j;

/**
 * @author xethhung
 * @date 11/23/2017
 */
public interface StandaloneTask {
    int execute(CommandExtractor4j args);
}
