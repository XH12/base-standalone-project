package com.xtools.standalone;
import com.xtools.extractor.command.CommandExtractor4j;
import com.xtools.standalone.sys.StandaloneTask;
import com.xtools.standalone.sys.logUtils.WithLoggerImpl;
import org.springframework.stereotype.Component;

/**
 * @author xethhung
 * @date 11/24/2017
 */
@Component("task")
public class Task extends WithLoggerImpl implements StandaloneTask{
    @Override
    public int execute(CommandExtractor4j args) {
        logger.info("This is a testing task");
        return 0;
    }
}
